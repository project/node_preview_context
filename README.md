INTRODUCTION
------------

This module support fix issue with node context condition in preview page

https://www.drupal.org/project/drupal/issues/2890758

INSTALLATION
------------

Install the module as you would normally install a contributed Drupal
module. Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
--------------

Just install module and have fun.
