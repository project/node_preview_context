<?php

namespace Drupal\node_preview_context;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\node_preview_context\ContextProvider\NodeRouteContext;

/**
 * Class NodePreviewContextServiceProvider.
 *
 * @package Drupal\node_preview_context
 */
class NodePreviewContextServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $definition = $container->getDefinition('node.node_route_context');
    $definition->setClass(NodeRouteContext::class);
  }

}
