<?php

namespace Drupal\node_preview_context\ContextProvider;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Plugin\Context\Context;
use Drupal\Core\Plugin\Context\EntityContextDefinition;
use Drupal\node\ContextProvider\NodeRouteContext as NodeRouteContextBase;

/**
 * Sets the current node as a context on node routes.
 */
class NodeRouteContext extends NodeRouteContextBase {

  /**
   * {@inheritdoc}
   */
  public function getRuntimeContexts(array $unqualified_context_ids) {
    $value = NULL;
    if (($route_object = $this->routeMatch->getRouteObject()) && ($route_contexts = $route_object->getOption('parameters')) && (isset($route_contexts['node_preview']))) {
      if ($node = $this->routeMatch->getParameter('node_preview')) {
        $value = $node;
      }
    }
    if ($value) {
      $result = [];
      $context_definition = EntityContextDefinition::create('node')->setRequired(FALSE);
      $cacheability = new CacheableMetadata();
      $cacheability->setCacheContexts(['route']);

      $context = new Context($context_definition, $value);
      $context->addCacheableDependency($cacheability);
      $result['node'] = $context;

      return $result;
    }
    return parent::getRuntimeContexts($unqualified_context_ids);
  }

}
